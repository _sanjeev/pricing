// const fun = (() => {
//     if (document.getElementById ('basicPrice').innerText === '$199.99') {
//         document.getElementById ('basicPrice').innerText = '$19.99';
//     }else if (document.getElementById ('basicPrice').innerText === '$19.99') {
//         document.getElementById ('basicPrice').innerText = '$199.99';
//     }

//     if (document.getElementById ('professionalPrice').innerText === '$249.99') {
//         document.getElementById ('professionalPrice').innerText = '$24.99'
//     }else if (document.getElementById ('professionalPrice').innerText === '$24.99') {
//         document.getElementById ('professionalPrice').innerText = '$249.99'
//     }

//     if (document.getElementById ('masterPrice').innerText === '$399.99') {
//         document.getElementById ('masterPrice').innerText = '$39.99';
//     }else if (document.getElementById ('masterPrice').innerText === '$39.99') {
//         document.getElementById ('masterPrice').innerText = '$399.99';
//     }
// })
const getData = (() => {
    fetch ('./data.json').then ((response) => {
        return response.json();
    }).then ((data) => {
        // console.log(data);
        fun (data);
    }).catch ((err) => {
        console.log(err);
    })
})


const fun = ((data) => {
    const input = document.querySelector ('input');
    if (input.checked) {
        document.getElementById ('basicPrice').innerText = `${data.monthly.basic}`;
        document.getElementById ('professionalPrice').innerText = `${data.monthly.professional}`;
        document.getElementById ('masterPrice').innerText = `${data.monthly.master}`;
    }else {
        document.getElementById ('basicPrice').innerText = `${data.annually.basic}`;
        document.getElementById ('professionalPrice').innerText = `${data.annually.professional}`;
        document.getElementById ('masterPrice').innerText = `${data.annually.master}`;
    }
})

// getData ('./data.json');

